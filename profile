# 
# profile-common
#
umask 007

#
# base
#
export LANG=en_US.UTF-8
export PROJECT_ID="${PROJECT_ID:-UNKNOWN_PROJECT}"
export H="${H:-/opt/${PROJECT_ID}}"
export VAR="${VAR:-/var/${PROJECT_ID}}"
export LOG="${VAR}/log"
export SYS="${H}/sys"
export ETC="${H}/etc"
export PKG="${H}/pkg"
export LIB="${H}/lib"
export MOD="${H}/mod"

export MANPATH="${MANPATH:-/usr/share/man}"

eval `"${H}/bin/env.sh"`

export ENV=development
EF="${ETC}/env/`hostname`"
test -e "${EF}" && ENV=`cat ${EF}`

# runit base
export SVBASE="${ETC}/sv"

# postgresql
#
# this must be set globally here,
# so the pg CLI tools know where
# to look.
export PGDATA="${VAR}/postgresql/pgdata"
export PGHOST="127.0.0.1"
export PGUSER="jc"
export PGDATABASE="jc"

# ensure base tree
mkdir -p "${H}/sys/opt"
mkdir -p "${VAR}/cfg"

# ${H}/var -> ${VAR} (for symlinks)
test -e "${H}/var" || ln -sf "${VAR}" "${H}/var"

if test -d "${H}/mod"
then
  # add mod/*/bin to PATH
  if ls ${H}/mod/*/bin >/dev/null 2>&1
  then
    for p in ${H}/mod/*/bin
    do
      export PATH=${PATH}:$p
    done
  fi

  # source mod/*/.profile
  if ls ${H}/mod/*/.profile >/dev/null 2>&1
  then
    for p in ${H}/mod/*/.profile
    do
      test "$p" == "${H}/mod/*/.profile" && continue
      source $p
    done
  fi
fi

# declare a home-zone
cd() {
if test -z "$1"
then
  cd ${H}
else
  builtin cd "$@"
fi
}

if test "${PS1}"
then
  # interactive terminal

  # some useful alias(es)
  alias h="cd ${HOME}"

  # make sure bin/gs is used
  unalias gs 2>/dev/null

  # tree view
  lt(){
#    ls -R $1 | grep ":$" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/'
    ls -p $(ls -R $(dirname $1)/$(basename $1) | grep ":$" | sed -e 's/:$//') | grep -v "/$" | sed -n -e '/^$/d' -e '/:$/{h;p}' -e '/[^:][^:]*:$/!{G;s@\(.*\)\n\(.*\):@\2/\1@g;p}' | sed -n -e '1{h;p}' -e '1!{G;s@\(.*\)\n\([^:]*\):@\2\n\1@g;s@\(.*\)\n\1/@/@;s@[^/]*/@    @g;p}'
  }
fi

true
