#!/bin/bash

die() {
  echo $1
  exit 1
}

read PACKAGE URL ARCHURL LATEST WORKING ARCHIVE TGT ALGO HASH
test "PACKAGE" = "${PACKAGE}" || die "PACKAGE ${PACKAGE}"
test "URL" = "${URL}" || die "URL ${URL}"
test "ARCHURL" = "${ARCHURL}" || die "ARCHURL ${ARCHURL}"
test "LATEST" = "${LATEST}" || die "LATEST ${LATEST}"
test "WORKING" = "${WORKING}" || die "WORKING ${WORKING}"
test "ARCHIVE" = "${ARCHIVE}" || die "ARCHIVE ${ARCHIVE}"
test "TGT" = "${TGT}" || die "TGT ${TGT}"

while read PACKAGE URL ARCHURL LATEST WORKING ARCHIVE TGT ALGO HASH
do
  echo $WORKING
  FETCH="$(eval echo ${ARCHURL})$(eval echo ${ARCHIVE})"
  TGT="$(eval echo ${TGT})"
  test -s "${TGT}" && continue
  echo "fetch: ${FETCH} tgt: ${TGT}"
  curl --disable-epsv -skL "${FETCH}" > "${TGT}"
done
