#!/usr/bin/env python

"""
build.py, generic prerequisite build management

Copyright (c) 2010-2012 Moritz Rill.
Copyright (c) 2012-2016 Ingo Struck. copyright@ingostruck.de
May be used according to the accompanying MIT LICENSE packaged 
with this file. Please send a note to copyright@ingostruck.de 
prior to using if you are unable to locate or comply with LICENSE.
"""

import sys,os,glob,shutil,hashlib
from subprocess import Popen, PIPE
from datetime import datetime

sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

def run( cmd, env=os.environ ):
    cmd = ['/bin/bash', '-c', 'eval `${H}/bin/env.sh` ; source $0 $@'] + cmd
    p = Popen( cmd, shell=False, stdout=PIPE, stderr = PIPE, env = env)
    out, err = p.communicate()
    return ( p.returncode, out, err )


home = os.environ.get( 'SYS' )

if not home:
    print "environment variable SYS is not set"
    sys.exit(1)

opt = os.path.join( home, 'opt' )

try:
    os.makedirs( opt )
except:
    pass

try:
    os.mkdir( 'BUILD' )
except:
    pass

os.chdir( 'BUILD' )

try:
    os.mkdir( 'tmp' )
except:
    pass

os.chdir( 'tmp' )

recipes = glob.glob( os.path.join( '..', '..', '*.recipe' ) )
recipes.sort()

os.chdir( '..' )
os.rmdir( 'tmp' )

c=len(recipes)
i=0

stamp = datetime.now().strftime('%Y%m%d%H%M%S')

print "build destination: %s" % ( home )

for r in recipes:
    archname = os.path.basename(r)[:-7]
    archname = archname[archname.find('-')+1:]

    try:
        shutil.rmtree( archname, ignore_errors=True )
    except:
        pass

    try:
        os.mkdir( archname )
    except:
        pass

    os.chdir( archname )
    d = {}

    f = open( r )

    m = hashlib.sha224()
    m.update(f.read())
    h = m.hexdigest()
    h = h[0:3] + h[-3:]

    f.seek(0)
    while True:
        l = f.readline()
        if '=' not in l:
            break
        k,v = l.split( '=' )
        d[k.strip()] = v.strip()
    f.close()

    symlink = None
    symtxt  = ''

    if 'SYMLINK' in d:
        symlink = d['SYMLINK']
        symtxt = ' (%s)' % (symlink)

    myenv = os.environ.copy()
    target = os.path.join( opt, "%s-%s" % ( archname, h ) )
    myenv['ARCHIVE'] = os.path.join( '..', '..', archname )
    myenv['PREFIX']  = "%s-%s" % (target, stamp)

    idx    = "[%2d/%2d]" % ( i+1, c )
    indent = ' ' * len( idx )
    arrow  = '=' * ( len( idx ) - 1 ) + '>'
    start = datetime.now()
    print "%s check: %s%s at %s" % ( idx, archname, symtxt, start.strftime('%Y%m%d%H%M%S') )

    installed = False
    if not symlink:
        # DBG print "no symlink installation check"
        ret, out, err = run( [ r, 'is_installed' ], env = myenv )
        # DBG print "installation check returns %s %s %s" % (ret, out, err)

        if 10 == ret:
            installed = True
    else:
        # DBG print "symlink %s" % (symlink)
        full_link = os.path.join( home, symlink )
        if os.path.exists( full_link ):
            old_target = os.readlink( full_link )
            if old_target.startswith(target):
                installed = True

    target = "%s-%s" % (target, stamp)
    check = datetime.now()
    print "%s check: %s%s took %s" % ( idx, archname, symtxt, check - start )

    if installed:
        print "%s  skip: %s%s already installed" % ( idx, archname, symtxt )
        os.chdir( '..' )
        i+=1
        continue

    print "%s build: %s%s" % ( idx, archname, symtxt )

    ret, out, err = run( [ r ], env=myenv )

    if 0 < len( out ):
        print "%s WARNING: Output to stdout detected, see BUILD/%s/stdout" % ( arrow, archname )
        f = open( 'stdout', 'w' )
        f.write( out )
        f.close()

    if 0 < len( err ):
        print "%s WARNING: Output to stderr detected, see BUILD/%s/stderr" % ( arrow , archname )
        f = open( 'stderr', 'w' )
        f.write( err )
        f.close()

    os.chdir( '..' )

    if not 0 == ret:
        pager = os.getenv('PAGER', 'cat')
        os.system("%s %s/build.log" % (pager, archname))
        print "\n*** ERROR, exit code %d. See: BUILD/%s/build.log" % ( ret, archname )
        print
        sys.exit(1)

    if symlink:
        try:
            os.unlink( os.path.join( home, symlink ) )
            print "%s UPDATE: %s => %s" % ( indent, os.path.join( home, symlink ), target )
        except:
            print "%s install: %s => %s" % ( indent, os.path.join( home, symlink ), target )

        os.symlink( target, os.path.join( home, symlink ) )

    time = datetime.now()
    print "%s build: %s%s took %s total %s" % ( idx, archname, symtxt, time - check, time - start )
    i+=1


