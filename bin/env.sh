#!/bin/bash

# Patches the environment according
# to pkg/*.recipe, rooted at ${SYS}
#
# Synopsis:
#   export SYS=~/sys
#   source pkg/env.sh

if test -n "${SYS}" -a  -n "${PKG}"
then
	WHICH=$(which which)
	BN=$(which basename)
	SED=$(which sed)
	PYTHON=$(which python)
	SP="C_INCLUDE_PATH CPATH CPLUS_INCLUDE_PATH LIBRARY_PATH LD_LIBRARY_PATH DYLD_LIBRARY_PATH MANPATH PKG_CONFIG_PATH PATH PYTHONPATH LUA_PATH LUA_CPATH LUA_PATH_JAIL LUA_CPATH_JAIL CORES LUA_ABI DISPLAY"
	for V in ${SP} ; do export ${V}="" ; done
	# note: have EMPTY PATH from now on

	# first pass: extract PATH
	for P in ${SYS}/*
	do
		if test -d "${P}/bin"
		then
			PATH="${PATH}:${P}/bin"
		fi
		if test -d "${P}/sbin"
		then
			PATH="${PATH}:${P}/sbin"
		fi
	done

	# check some local ABI versions
	LPYTHON=`${WHICH} python`
	PYTHON=${LPYTHON:-$PYTHON}
	PY_ABI=`${PYTHON} -V 2>&1 | ${SED} -e 's/^P/p/;s/ \([0-9]\)\.\([0-9]\).*$/\1.\2/'`
	LUA_ABI=$(${WHICH} lua >/dev/null 2>&1 && lua -e 'print(_VERSION:match("[0-9]+%.[0-9]"))')

	# add local bin
	if test -d ${H}/bin
	then
		PATH="${H}/bin${PATH}"
	fi

	# add "allowed" external deps
	PATH="${PATH}:/bin:/usr/bin"

	# DBG: echo "echo \"PYTHON ABI $PY_ABI\" ;"
	# DBG: echo "echo \"LUA ABI $LUA_ABI\" ;"
	# TODO: check if we really want CWD in LUA_PATH / LUA_CPATH
	LUA_PATH="./?.lua;${SYS}/orsty/lualib/?.lua"
	LUA_CPATH="./?.so;${SYS}/orsty/lualib/?.so"
	LUA_PATH_JAIL="./?.lua;/sys/lualib/?.lua"
	LUA_CPATH_JAIL="./?.so;/sys/lualib/?.so"

	# second pass: set path like stuff that may depend on ABI versions
	for P in ${SYS}/*
	do
		JH=${P##${H}}
		if test -d "${P}/include"
		then
			C_INCLUDE_PATH="${C_INCLUDE_PATH}:${P}/include"
			CPATH="${C_INCLUDE_PATH}:${P}/include"
			CPLUS_INCLUDE_PATH="${CPLUS_INCLUDE_PATH}:${P}/include"
		fi
		if test "${P}" != "${SYS}/openssl" -a -d "${P}/lib"
		then
			LIBRARY_PATH="${LIBRARY_PATH}:${P}/lib"
			LD_LIBRARY_PATH="${LIBRARY_PATH}:${P}/lib"
			DYLD_LIBRARY_PATH="${LIBRARY_PATH}:${P}/lib"
		fi
		if test -d "${P}/man"
		then
			MANPATH="${MANPATH}:${P}/man"
		fi
		if test -d "${P}/lib/pkgconfig"
		then
			PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:${P}/lib/pkgconfig"
		fi
		if test -d "${P}/luajit/lib/pkgconfig"
		then
			PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:${P}/luajit/lib/pkgconfig"
		fi
		if test -d "${P}/lib/${PY_ABI}/site-packages"
		then
			PYTHONPATH="${PYTHONPATH}:${P}/lib/${PY_ABI}/site-packages"
		fi
		if test -d "${P}/lib/lua/${LUA_ABI}"
		then
			LUA_CPATH="${LUA_CPATH};${P}/lib/lua/${LUA_ABI}/?.so"
			LUA_CPATH_JAIL="${LUA_CPATH_JAIL};${JH}/lib/lua/${LUA_ABI}/?.so"
		fi
		if test -d "${P}/share/lua/${LUA_ABI}"
		then
			LUA_PATH="${LUA_PATH};${P}/share/lua/${LUA_ABI}/?.lua;${P}/share/lua/${LUA_ABI}/?/init.lua"
			LUA_PATH_JAIL="${LUA_PATH_JAIL};${JH}/share/lua/${LUA_ABI}/?.lua;${JH}/share/lua/${LUA_ABI}/?/init.lua"
		fi
	done

	# number of cores for -j build flag
	CORES=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || sysctl -n hw.ncpu || psrinfo -p || echo "1")

	# add "allowed" external deps
  MANPATH="${MANPATH}:/usr/share/man"

	# export
	for K in ${SP} ; do V=${!K} ; V=${V##:} ; printf "export %s=\"%s\"\n" "${K}" "${V}" ; done

	# X display
	printf "export DISPLAY=\":0\"\n"

fi
# EOF env.sh
