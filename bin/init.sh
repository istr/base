export PROJECT_ID=jubbr
export APP_TLD="${PROJECT_ID}.com"
export H="${PWD}"
export VAR="${PWD}/var"
export TEMP="${PWD}/var/tmp"
export USER="$(id -nu)"
export COPYRIGHT="(c) $(date +%Y) Jubbr AG. All rights reserved."

. ${PWD}/profile

if shopt -q login_shell
then
	if test "${PS1}"
	then
	  E="\e[0;30;47m development \e[0m"
	  test "development" != "${ENV}" && E="\e[0;1;41;33m ${ENV} \e[0m"
	  echo
	  echo -e "\e[0;40;37;1m ${PROJECT_ID} ${E}\e[0m in \e[1m${H} \e[0m@ \e[1m${APP_TLD}"
	  echo
	  echo -en "\e[30;1m"
	  PAD=
	  ${HOME}/bin/rl | sed "s/\*\([^ ]\+\)/[32;1m*\1[30;1m/;s/^/${PAD}/"
	  echo -e "\e[0m"
	else
	  echo "entering: ${PROJECT_ID} '$PS1'"
	fi
fi
