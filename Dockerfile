# minimal dockerfile for jubbr lua api
FROM registry.gitlab.com/istr/build:0.3.0
MAINTAINER istr <docker@ingostruck.de>

# Ensure UTF-8
ENV LANG       en_US.UTF-8
ENV LC_ALL     en_US.UTF-8

# Change this ENV variable to skip the docker cache from this line on
ENV LATEST_CACHE 2016-09-17T12:00-00:00

# Copy pre-built stuff from git
COPY pushd/ /opt/jc/mod/pushd

# Copy built stuff
COPY sys/ /opt/jc/sys

# install packages we use from distro
RUN apk update
RUN apk add nodejs

# install coffeescript for pushd
RUN npm -g install coffee-script

# set host name
RUN echo -ne '127.0.0.1 localhost\n127.0.1.1 virtual' | tee /etc/hosts
RUN echo 'virtual' | tee /etc/hostname

# build the ujail binary
RUN NEW_ROOT=/opt/jc /bin/bash -c 'cd ${NEW_ROOT} && bin/mkujail.sh'